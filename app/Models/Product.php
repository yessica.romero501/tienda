<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public $fillable= ['id','name','description','price','active'];

   public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tickets()
    {
        return  $this->belongsTo(Ticket::class);

    }
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }


}


