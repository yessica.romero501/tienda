<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoryproduct extends Model
{
    use HasFactory;
    public $fillable= ['id','product_id','category_id'];
    public $table='category_product';

}
