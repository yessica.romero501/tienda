<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    public $fillable= ['id','piece','amount','product_id','ordcomp_id'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function ordcomp()
    {
        return $this->belongsTo(Ordcomp::class);
    }
}
