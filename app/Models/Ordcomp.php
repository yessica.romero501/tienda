<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ordcomp extends Model
{
    use HasFactory;
    public $fillable= ['id','total','user_id','status_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }
}
