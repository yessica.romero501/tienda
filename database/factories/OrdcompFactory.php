<?php

namespace Database\Factories;

use App\Models\Ordcomp;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrdcompFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ordcomp::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'total'=>$this->faker->randomNumber(3),
            'user_id'=> User::all()->random(),
            'status_id'=>Status::all()->random()

        ];
    }
}
