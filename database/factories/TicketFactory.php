<?php

namespace Database\Factories;

use App\Models\Ordcomp;
use App\Models\Product;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ticket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'piece'=>$this->faker->randomNumber(3),
            'amount'=>$this->faker->randomNumber(3),
            'product_id'=> Product::all()->random(),
            'ordcomp_id'=> Ordcomp::all()->random()
        ];
    }
}
