<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Categoryproduct;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryproductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Categoryproduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id'=> Product::all()->random(),
            'category_id'=> Category::all()->random()
        ];
    }
}
