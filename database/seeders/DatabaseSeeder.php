<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Category;
use App\Models\Ordcomp;
use App\Models\Product;
use App\Models\Status;
use App\Models\Photo;
use App\Models\Categoryproduct;
use App\Models\Ticket;
use Illuminate\Database\Seeder;
use NunoMaduro\Collision\Adapters\Phpunit\State;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        Product::factory(10)->create();
        Status::factory(10)->create();
        Category::factory(10)->create();
        Photo::factory(10)->create();
        Categoryproduct::factory(10)->create();
        Ordcomp::factory(10)->create();
        Ticket::factory(10)->create();

    }
}
