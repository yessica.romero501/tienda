<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->integer('piece')->unsigned();
            $table->decimal('amount', 9,2)->unsigned();
            $table->tinyInteger('active')->unsigned()->default(1);
            $table->foreignId('product_id')->index()
                ->constrained('products')
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->foreignId('ordcomp_id')->index()
                ->constrained('ordcomps')
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
