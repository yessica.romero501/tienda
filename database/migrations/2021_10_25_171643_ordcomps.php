<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ordcomps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordcomps', function (Blueprint $table) {
            $table->id();
            $table->decimal('total', 9,2)->unsigned();
            $table->foreignId('user_id')->index()
                ->constrained('users')
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->foreignId('status_id')->index()
                ->constrained('statuses')
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordcompr');
    }
}
