@extends('layouts.app')

@section('content')
    <h1>Mostrar Producto {{$Product->id}}</h1>

        <p>nombre {{ $Product->name }}</p>
        <p>descripcion {{ $Product->description}}</p>
        <p>precio {{$Product->price}} </p>
        <p>sku {{$Product->sku}}</p>
        <h2>aqui van las fotos </h2>

        @foreach ($Product->photos as $photo )
            <p><img src="{{asset('/photos/'.$photo->name)}}" alt="{{$photo->name}}"></p>

        @endforeach
@endsection
