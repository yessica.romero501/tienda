@extends('layouts.app')
@section('content')

    <h1>Actualizar Producto</h1>
    <form action="{{route('product.update',[$Product->id])}}" method="post">
        @csrf
        {{ method_field('PUT') }}
        <p>nombre<input type="text" name="name" value="{{ $Product->name }}"></p>
        <p>descripcion<input type="text" name="description" value="{{ $Product->description}}"></p>
        <p>precio<input type="text" name="price"  value="{{$Product->price}}"></p>
        <p>sku<input type="text" name="sku" value="{{$Product->sku}}"></p>
        <p><button>enviar</button></p>
    </form>
@endsection

