@extends('layouts.app')
@section('content')

    <h1>Crear producto</h1>
        <form action="{{route('product.store')}}" method="post">
            @csrf
            <p>nombre<input type="text" name="name"></p>
            <p>descripcion<input type="text" name="description"></p>
            <p>precio<input type="text" name="price"></p>
            <p>sku<input type="text" name="sku"></p>
            <p><button>enviar</button></p>
        </form>
@endsection
