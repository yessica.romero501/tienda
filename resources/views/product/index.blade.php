@extends('layouts.app')
@section('content')
    <h1>Productos</h1>
    <table>
        <thead>
            <tr>
                <th>id</th>
                <th>nombre</th>
                <th>descripcion</th>
                <th>precio</th>
                <th>sku</th>
                <th>editar</th>

            </tr>
        </thead>
        <tbody>
            @foreach ( $Products as $Product )
            <tr>
                <td>{{ $Product->id }}</td>
                <td>{{ $Product->name}}</td>
                <td>{{$Product->description}}</td>
                <td>{{$Product->price}}</td>
                <td>{{ $Product->sku }}</td>
                <td><a href="{{route('product.edit',[$Product->id])}}"> Editar</a></td>
                <td><a href="{{route('product.show',[$Product->id])}}"> Mostrar </a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endsection

